export enum ROUTES {
  INDEX = "/",
  LOGIN = "/login",
  SIGNUP = "/signup",
  MEASUREMENTS = "/measurements",
  EXERCISE_TYPES = "/exercise-types",
  EXERCISES = "/exercises",
  WORKOUTS = "/workouts",
  PROFILE = "/profile",
  NOT_FOUND = "/*",
}

export enum ROLE {
  ADMIN = "admin",
  USER = "user",
}

export enum GENDER {
  FEMALE = "female",
  MALE = "male",
}

export enum EXERCISETYPE {
  PECTORALS = "pectorals",
  DELTOIDS = "deltoids",
  BICEPS = "biceps",
  TRICEPS = "triceps",
  DELTS = "delts",
  QUADRICEPS = "quadriceps",
  GLUTES = "glutes",
  HAMSTRINGS = "hamstrings",
  CALVES = "calves",
  LATS = "lats",
  TRAPEZIUS = "trapezius",
  ABDOMINALS = "abdominals",
  OBLIQUES = "obliques",
  RHOMBOIDS = "rhomboids",
  SHOULDERS = "shoulders",
}

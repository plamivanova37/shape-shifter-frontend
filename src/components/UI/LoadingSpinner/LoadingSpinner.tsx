import "./LoadingSpinner.scss";

const LoadingSpinner = () => {
  return (
    <div className="spinner__container">
      <div className="spinner"></div>
    </div>
  );
};

export default LoadingSpinner;
